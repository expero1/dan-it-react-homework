export const definitions = {
  showModal: "showModal",
  hideModal: "hideModal",
  modalContent: "modalContent",
};
