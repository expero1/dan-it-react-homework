import { Button } from "../button/Button";

export const modalContent = [
  {
    id: 0,
    header: "Do You want to Delete Thes File",
    text: "Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?",
    closeButton: true,
    action: [
      <Button
        backgroundColor="#9c3a2b"
        text="Ok"
        onClick={() => {
          alert("ok");
        }}
      />,
      <Button
        backgroundColor="#9c3a2b"
        text="Cancel"
        onClick={() => {
          alert("cancel");
        }}
      />,
    ],
    buttonColor: "red",
    buttonText: "Show modal 1",
  },
  {
    id: 1,
    header: "header1",
    text: "text1",
    closeButton: false,
    action: [
      <Button
        backgroundColor="#9c3a2b"
        text="text2"
        onClick={() => {
          alert("cancel");
        }}
      />,
      <Button
        backgroundColor="green"
        text="text2"
        onClick={() => {
          alert("cancel");
        }}
      />,
    ],
    buttonColor: "green",
    buttonText: "Show modal 2",
  },
];
