import React from "react";
import "./App.css";
import { Button } from "./button/Button";
import { Modal } from "./modal/Modal";
import { definitions } from "./vars/definitions";
import { modalContent } from "./vars/modalContent";
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { modalContent: {} };
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }
  getModalContent(id) {
    return modalContent.find(({ id: modalId }) => modalId === Number(id));
  }
  showModal(e) {
    const modalId = e.target.dataset.id;
    this.setState({
      [definitions.modalContent]: this.getModalContent(modalId),
    });
  }
  hideModal() {
    this.setState({ [definitions.modalContent]: {} });
  }
  render() {
    return (
      <div className="App">
        {modalContent.map((content, index) => (
          <Button
            key={index}
            backgroundColor={content.buttonColor}
            text={`show modal ${content.id}`}
            onClick={this.showModal}
            dataSet={{ id: content.id }}
          />
        ))}

        {Object.keys(this.state[definitions.modalContent]).length > 0 && (
          <Modal
            {...this.state[definitions.modalContent]}
            closeCallback={this.hideModal}
          />
        )}
      </div>
    );
  }
}

export default App;
