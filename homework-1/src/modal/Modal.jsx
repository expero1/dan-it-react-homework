import React from "react";
import "./modal.scss";
export class Modal extends React.Component {
  constructor(props) {
    //header, text, closeButton, actions
    super(props);
    this.clickHandler = this.clickHandler.bind(this);
  }
  clickHandler({ target }) {
    (target.classList.contains("modal") ||
      target.classList.contains("close-btn")) &&
      this.props.closeCallback();
  }
  render() {
    const renderActionButtonsSection = function (actionButtons) {
      return !actionButtons
        ? []
        : actionButtons.map((actionButton, index) => (
            <li key={index}>{actionButton}</li>
          ));
    };
    return (
      <div className="modal" onClick={this.clickHandler}>
        <div className="modal__content">
          <div className="modal__header">
            {this.props.header}
            {this.props.closeButton && <button className="close-btn"></button>}
          </div>
          <div className="modal__body">{this.props.text}</div>
          <ul className="modal-button__block">
            {[...renderActionButtonsSection(this.props.action)]}
          </ul>
        </div>
      </div>
    );
  }
}
