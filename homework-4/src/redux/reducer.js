import { combineReducers } from "redux";
import {
  ADD_TO_CART,
  FETCH_PRODUCTS_REQUEST,
  FETCH_PRODUCTS_SUCCESS,
  HIDE_MODAL,
  REMOVE_FROM_CART,
  SHOW_MODAL,
  TOGGLE_PRODUCT_IN_WISH_LIST,
} from "./action";
import {
  getCartFromLocalStorage,
  getWishListFromLocalStorage,
} from "../services/local-storage";

const productsInitialState = {
  isFetching: false,
  products: [],
  isProductsLoaded: false,
};
const cartInitialState = [...getCartFromLocalStorage()];
const wishListInitialState = [...getWishListFromLocalStorage()];
const modalInitialState = {};

const addToCard = (currentCart, productId) => {
  const currentCartSet = new Set(currentCart);
  return Array.from(currentCartSet.add(productId));
};

const removeFromCart = (currentCart, productId) => {
  return currentCart.filter((id) => id !== productId);
};

const toggleProductInWishList = (currentWishList, productId) => {
  const wishListSet = new Set(currentWishList);
  wishListSet.has(productId)
    ? wishListSet.delete(productId)
    : wishListSet.add(productId);
  return Array.from(wishListSet);
};

/* REDUCERS */
const productReducer = (state = productsInitialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS_REQUEST:
      return {
        ...state,
        isFetching: true,
        products: [],
      };
    case FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        products: action.payload,
        isProductsLoaded: true,
      };
    default:
      return state;
  }
};
const cartReducer = (state = cartInitialState, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      return addToCard(state, action.payload);
    case REMOVE_FROM_CART:
      return removeFromCart(state, action.payload);
    default:
      return state;
  }
};

const wishListReducer = (state = wishListInitialState, action) => {
  switch (action.type) {
    // case ADD_TO_WISHLIST:
    //   return addToWishList(state, action.payload);
    // case REMOVE_FROM_WISHLIST:
    //   return removeFromWishList(state, action.payload);

    case TOGGLE_PRODUCT_IN_WISH_LIST:
      return toggleProductInWishList(state, action.payload);
    default:
      return state;
  }
};
const modalReducer = (state = modalInitialState, action) => {
  switch (action.type) {
    case SHOW_MODAL:
      return { ...action.payload };
    case HIDE_MODAL:
      return {};
    default:
      return state;
  }
};

export const rootReducer = combineReducers({
  products: productReducer,
  modal: modalReducer,
  cart: cartReducer,
  wishList: wishListReducer,
});
