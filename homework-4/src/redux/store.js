import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
import { rootReducer } from "./reducer";
import {
  setCartToLocalStorage,
  setWishListToLocalStarage,
} from "../services/local-storage";
export const store = createStore(rootReducer, applyMiddleware(thunk, logger));
store.subscribe(() => {
  setCartToLocalStorage(store.getState().cart);
});
store.subscribe(() => {
  setWishListToLocalStarage(store.getState().wishList);
});
