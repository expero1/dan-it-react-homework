import { getProducts } from "../services/get-products";
export const ADD_TO_CART = "ADD-TO-CART";
export const REMOVE_FROM_CART = "REMOVE-FROM-CART";
export const TOGGLE_PRODUCT_IN_WISH_LIST = "TOGGLE-PRODUCT-IN-WISHLIST";
export const SHOW_MODAL = "SHOW-MODAL";
export const HIDE_MODAL = "HIDE-MODAL";
export const FETCH_PRODUCTS_REQUEST = "FETCH-PRODUCTS-REQUEST";
export const FETCH_PRODUCTS_SUCCESS = "FETCH-PRODUCTS-SUCCESS";
export const FETCH_PRODUCTS_ERROR = "FETCH-PRODUCTS-ERROR";

export const addToCartAction = (productId) => {
  return {
    type: ADD_TO_CART,
    payload: productId,
  };
};
export const removeFromCartAction = (productId) => {
  return {
    type: REMOVE_FROM_CART,
    payload: productId,
  };
};
export const showModalAction = ({
  header,
  text,
  closeButton,
  closeCallback,
  action,
}) => {
  return {
    type: SHOW_MODAL,
    payload: { header, text, closeButton, closeCallback, action },
  };
};

export const hideModalAction = () => {
  return {
    type: HIDE_MODAL,
  };
};

export const toggleProductInWishListAction = (productId) => {
  return {
    type: TOGGLE_PRODUCT_IN_WISH_LIST,
    payload: productId,
  };
};

export const fetchProductsStartAction = () => {
  return {
    type: FETCH_PRODUCTS_REQUEST,
  };
};

export const fetchProductsSuccessAction = (productList) => {
  return {
    type: FETCH_PRODUCTS_SUCCESS,
    payload: productList,
  };
};

export const fetchProductsErrorAction = (error) => {
  return {
    type: FETCH_PRODUCTS_ERROR,
    payload: error,
  };
};

export const fetchProductsAction = () => async (dispatch) => {
  dispatch(fetchProductsStartAction());
  const products = await getProducts();
  dispatch(fetchProductsSuccessAction(products));
};
