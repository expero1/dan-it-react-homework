import React from "react";
import "./modal.scss";
import { CloseButton } from "../close-button/close-button";
// import { connect } from "react-redux";
import { useSelector } from "react-redux";
function clickHandler(e, closeCallback) {
  if (
    e.target.classList.contains("modal") ||
    e.target.classList.contains("close-btn")
  ) {
    closeCallback();
  }
}

function renderActionButtonsSection(actionButtons) {
  return !actionButtons
    ? []
    : actionButtons.map((actionButton, index) => (
        <li key={index}>{actionButton}</li>
      ));
}
export default function Modal(props) {
  const isModalOpen = useSelector(
    (state) => Object.keys(state.modal).length > 0
  );
  const modalContent = useSelector((state) => state.modal);
  if (!isModalOpen) return null;
  const { header, text, closeButton, action, closeCallback } = modalContent;
  return (
    <div
      className="modal"
      onClick={(e) => {
        clickHandler(e, closeCallback);
      }}>
      <div className="modal__content">
        <div className="modal__header">
          {header}
          {closeButton && <CloseButton />}
        </div>
        <div className="modal__body">{text}</div>
        <ul className="modal-button__block">
          {[...renderActionButtonsSection(action)]}
        </ul>
      </div>
    </div>
  );
}

// const mapStateToProps = (state) => {
//   return {
//     modalContent: state.modal,
//     isModalOpen: Object.keys(state.modal).length > 0,
//   };
// };

// export default connect(mapStateToProps)(Modal);
