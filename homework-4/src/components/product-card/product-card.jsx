import { useDispatch, useSelector } from "react-redux";
import addToCardModal from "../../modal-content/add-to-card-modal";
import { toggleProductInWishListAction } from "../../redux/action";
import { BuyButton } from "../buy-button/buy-button";
import { WishListButton } from "../wish-list-button/wish-list-button";
import ProductInfo from "../product-info/product-info";
import { CloseButton } from "../close-button/close-button";
import "./product-card.scss";
import removeProductFromCartModal from "../../modal-content/remove-from-cart-modal";
export default function ProductCard({ productInfo, closeButton = false }) {
  const isProductInWishList = useSelector((state) =>
    state.wishList.includes(productInfo.id)
  );
  const isProductInCart = useSelector((state) =>
    state.cart.includes(productInfo.id)
  );

  const dispatch = useDispatch();

  return (
    <>
      {closeButton && (
        <CloseButton
          className="product-card-close-btn"
          onClick={() => {
            removeProductFromCartModal(productInfo.id);
          }}
        />
      )}
      <ProductInfo {...productInfo} />
      <WishListButton
        isProductInWishList={isProductInWishList}
        toggleWishListCallback={() => {
          dispatch(toggleProductInWishListAction(productInfo.id));
        }}
      />
      <BuyButton
        addToCardCallback={() => {
          addToCardModal(productInfo.id);
        }}
        isProductInCart={isProductInCart}
      />
    </>
  );
}
