import Button from "../button/button";
export const BuyButton = ({ isProductInCart, addToCardCallback }) => {
  return (
    <>
      {isProductInCart ? (
        <Button backgroundColor="grey" text="Added To Cart" disabled={true} />
      ) : (
        <Button
          backgroundColor="green"
          text="Buy"
          onClick={addToCardCallback}
        />
      )}
    </>
  );
};
