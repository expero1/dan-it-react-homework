import React from "react";
import "./button.scss";
export default function Button({
  backgroundColor,
  text,
  onClick,
  dataSet,
  disabled = false,
}) {
  return (
    <button
      className="btn"
      style={{ backgroundColor }}
      onClick={onClick}
      {...renderDataAttributes(dataSet)}
      disabled={disabled}>
      {text}
    </button>
  );
}

function renderDataAttributes(dataSet) {
  const renderedAttributes = {};
  for (let attribute in dataSet) {
    renderedAttributes[`data-${attribute}`] = dataSet[attribute];
  }
  return renderedAttributes;
}
