import React from "react";
import PropTypes from "prop-types";
import "./product-info.scss";

export default function ProductInfo(props) {
  return (
    <>
      <div className="product-card__image">
        <img src={props.productImage} alt={props.productName} />
      </div>
      <h2 className="product-card__name">{props.productName}</h2>
      <ul className="product-card__props">
        <li className="product-card__price">Price: {props.productPrice} UAH</li>

        <li className="product-card__sku">SKU: {props.productSKU}</li>
        {props.productColor && (
          <li className="product-card__color">Color: {props.productColor}</li>
        )}
      </ul>
    </>
  );
}

ProductInfo.propTypes = {
  productId: PropTypes.symbol,
  productImage: PropTypes.string,
  productPrice: PropTypes.number,
  productSKU: PropTypes.string,
  productColor: PropTypes.string,
};
ProductInfo.defaultProps = {
  productImage: "https://placehold.co/600x400?text=No+Image",
  productColor: "",
};
