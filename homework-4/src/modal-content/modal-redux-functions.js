import {
  hideModalAction,
  removeFromCartAction,
  showModalAction,
  addToCartAction,
} from "../redux/action";
import { store } from "../redux/store";
const dispatch = store.dispatch;

export const showModal = (modalContent) => {
  dispatch(showModalAction(modalContent));
};
export function addToCard(productId) {
  dispatch(addToCartAction(productId));
}
export const removeFromCart = (productId) => {
  dispatch(removeFromCartAction(productId));
};
export const hideModal = () => {
  dispatch(hideModalAction());
};
