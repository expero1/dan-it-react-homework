import Button from "../components/button/button";
import { showModal, hideModal, removeFromCart } from "./modal-redux-functions";

export default function removeProductFromCartModal(id) {
  showModal({
    header: "Do You want to delete product from cart?",
    text: "Please Choose",
    closeButton: true,
    closeCallback: hideModal,
    action: [
      <Button
        backgroundColor="red"
        text="Remove From Cart"
        onClick={() => {
          removeFromCart(id);
          hideModal();
        }}
      />,
      <Button backgroundColor="grey" text="cancel" onClick={hideModal} />,
    ],
  });
}
