import Button from "../components/button/button";
import { showModal, hideModal, addToCard } from "./modal-redux-functions";
export default function addToCardModal(productId) {
  showModal({
    header: "Do You want to add product to Basket",
    text: "Please Choose",
    closeButton: true,
    closeCallback: hideModal,
    action: [
      <Button
        backgroundColor="green"
        text="add"
        onClick={() => {
          addToCard(productId);
          hideModal();
        }}
      />,
      <Button backgroundColor="grey" text="cancel" onClick={hideModal} />,
    ],
  });
}
