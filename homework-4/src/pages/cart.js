import { useSelector } from "react-redux";
import ProductList from "../components/product-list/product-list";
export default function Cart(props) {
  const cart = useSelector((state) => state.cart);

  const products = useSelector((state) =>
    state.products.products.filter(({ id }) => cart.includes(id))
  );
  const isProductsLoaded = useSelector(
    (state) => state.products.isProductsLoaded
  );
  return (
    <>
      <h1>Cart</h1>
      {products.length === 0 && isProductsLoaded && <h2>No Items in Cart</h2>}
      <ProductList products={products} closeButton={true} />
    </>
  );
}
