import ProductList from "../components/product-list/product-list";
import { useSelector } from "react-redux";
export default function Products(props) {
  //   console.log(props);
  const products = useSelector((state) => state.products.products);
  return (
    <>
      <h1>Products</h1>
      <ProductList products={products} />
    </>
  );
}
