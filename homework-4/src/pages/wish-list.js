import { useSelector } from "react-redux";
import ProductList from "../components/product-list/product-list";

export default function WishList(props) {
  const products = useSelector((state) =>
    state.products.products.filter(({ id }) => state.wishList.includes(id))
  );
  const isProductsLoaded = useSelector(
    (state) => state.products.isProductsLoaded
  );
  return (
    <>
      <h1>Wish List</h1>
      {products.length === 0 && isProductsLoaded && "No Items In Wish List"}
      <ProductList products={products} />
    </>
  );
}
