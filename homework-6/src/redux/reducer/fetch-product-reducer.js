import {
  FETCH_PRODUCTS_REQUEST,
  FETCH_PRODUCTS_SUCCESS,
} from "../action/fetch-products-action";

const productsInitialState = {
  isFetching: false,
  products: [],
  isProductsLoaded: false,
};

const productReducer = (state = productsInitialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS_REQUEST:
      return {
        ...state,
        isFetching: true,
        products: [],
      };
    case FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        products: action.payload,
        isProductsLoaded: true,
      };
    default:
      return state;
  }
};

export default productReducer;
