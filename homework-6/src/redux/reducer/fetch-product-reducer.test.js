import {
  fetchProductsStartAction,
  fetchProductsSuccessAction,
} from "../action/fetch-products-action";
import productReducer from "./fetch-product-reducer";

const initialState = {
  isFetching: false,
  products: [],
  isProductsLoaded: false,
};
const testProducts = [{ id: 0 }, { id: 1 }];
// let state = [];
describe("wish list reducer", () => {
  test("Should return initial state", () => {
    expect(productReducer(undefined, { type: undefined })).toEqual(
      initialState
    );
  });
  test("fetchProductsSuccessAction is work", () => {
    let state = { ...initialState };
    state = productReducer(state, fetchProductsSuccessAction(testProducts));
    expect(state.products).toEqual(testProducts);
    expect(state.isFetching).toEqual(false);
    expect(state.isProductsLoaded).toEqual(true);
  });
  test("fetchProductsStartAction", () => {
    let state = { ...initialState };
    state = productReducer(state, fetchProductsStartAction(testProducts));
    expect(state.products).toEqual([]);
    expect(state.isFetching).toEqual(true);
    expect(state.isProductsLoaded).toEqual(false);
  });
});
