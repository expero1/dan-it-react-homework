import { HIDE_MODAL, SHOW_MODAL } from "../action/modal-action";

const modalInitialState = {};

export const modalReducer = (state = modalInitialState, action) => {
  switch (action.type) {
    case SHOW_MODAL:
      return { ...action.payload };
    case HIDE_MODAL:
      return {};
    default:
      return state;
  }
};

export default modalReducer;
