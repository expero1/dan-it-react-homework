import wishListReducer from "./wish-list-reducer";
import { toggleProductInWishListAction } from "../action/wish-list-action";

const initialState = [];
// let state = [];
describe("wish list reducer", () => {
  test("Should return initial state", () => {
    expect(wishListReducer(undefined, { type: undefined })).toEqual(
      initialState
    );
  });
  test("toggle product in wishlist", () => {
    let state = [];
    state = wishListReducer(state, toggleProductInWishListAction(0));
    expect(state).toEqual([0]);
    state = wishListReducer(state, toggleProductInWishListAction(1));
    expect(state).toEqual([0, 1]);
    state = wishListReducer(state, toggleProductInWishListAction(1));
    expect(state).toEqual([0]);
  });
});
