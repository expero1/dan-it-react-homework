import cartReducer from "./cart-reducer";
import { addToCartAction, removeFromCartAction } from "../action/cart-action";
import { testSnapshot } from "../../test-helpers/test-helpers";
const initialState = [];
// let state = [];
describe("cart reducer", () => {
  test("Should return initial state", () => {
    expect(cartReducer(undefined, { type: undefined })).toEqual(initialState);
  });
  test("add to cart action", () => {
    let state = [];
    state = cartReducer(state, addToCartAction(0));
    expect(state).toEqual([0]);
    state = cartReducer(state, addToCartAction(1));
  });
  test("remove from cart", () => {
    let state = [0, 1];
    state = cartReducer(state, removeFromCartAction(0));
    expect(state).toEqual([1]);
    state = cartReducer(state, removeFromCartAction(2));
    expect(state).toEqual([1]);
  });
});
