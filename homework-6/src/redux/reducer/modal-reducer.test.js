import modalReducer from "./modal-reducer";
import { showModalAction, hideModalAction } from "../action/modal-action";
const mockFunction = jest.fn();
const modalData = {
  header: "test header",
  text: "test text",
  closeButton: true,
  closeCallback: mockFunction,
  action: ["test action"],
};
const initialState = {};
describe("modal reducer", () => {
  test("Should return initial state", () => {
    expect(modalReducer(undefined, { type: undefined })).toEqual(initialState);
  });
  test("show modal", () => {
    let state = {};
    state = modalReducer(state, showModalAction(modalData));
    expect(state).toEqual(modalData);
  });
  test("hide modal", () => {
    let state = {};
    state = modalReducer(state, showModalAction(modalData));
    state = modalReducer(state, hideModalAction());
    expect(state).toEqual(initialState);
  });
});
