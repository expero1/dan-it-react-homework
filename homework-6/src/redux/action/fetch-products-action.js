import { getProducts } from "../../services/get-products";

export const FETCH_PRODUCTS_REQUEST = "FETCH-PRODUCTS-REQUEST";
export const FETCH_PRODUCTS_SUCCESS = "FETCH-PRODUCTS-SUCCESS";
export const FETCH_PRODUCTS_ERROR = "FETCH-PRODUCTS-ERROR";

export const fetchProductsStartAction = () => {
  return {
    type: FETCH_PRODUCTS_REQUEST,
  };
};

export const fetchProductsSuccessAction = (productList) => {
  return {
    type: FETCH_PRODUCTS_SUCCESS,
    payload: productList,
  };
};

export const fetchProductsErrorAction = (error) => {
  return {
    type: FETCH_PRODUCTS_ERROR,
    payload: error,
  };
};

export const fetchProductsAction = () => {
  return function (dispatch) {
    dispatch(fetchProductsStartAction());
    return getProducts().then((productData) => {
      dispatch(fetchProductsSuccessAction(productData));
    });
  };
};
