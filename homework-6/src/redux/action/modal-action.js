export const SHOW_MODAL = "SHOW-MODAL";
export const HIDE_MODAL = "HIDE-MODAL";
export const showModalAction = ({
  header,
  text,
  closeButton,
  closeCallback,
  action,
}) => {
  return {
    type: SHOW_MODAL,
    payload: { header, text, closeButton, closeCallback, action },
  };
};

export const hideModalAction = () => {
  return {
    type: HIDE_MODAL,
  };
};
