const cartKey = "basket";
const wishListKey = "wish-list";
const productCardViewKey = "product-list-card-view";
const getValueByKeyFromLocalStorage = (key, defaultValue = []) => {
  const savedValue = localStorage.getItem(key);
  try {
    return savedValue ? Array.from(JSON.parse(savedValue)) : defaultValue;
  } catch (error) {
    console.log(error);
    return defaultValue;
  }
};
const saveToLocalStorage = (key, value) => {
  localStorage.setItem(key, JSON.stringify(value));
};
export const getCartFromLocalStorage = () =>
  getValueByKeyFromLocalStorage(cartKey).filter(
    (productId) => typeof productId === "number"
  );
export const setCartToLocalStorage = (cart) => {
  saveToLocalStorage(cartKey, cart);
};

export const getWishListFromLocalStorage = () =>
  getValueByKeyFromLocalStorage(wishListKey).filter(
    (productId) => typeof productId === "number"
  );
export const setWishListToLocalStarage = (wishList) => {
  saveToLocalStorage(wishListKey, wishList);
};

export const getCardViewFromLocalStorage = () => {
  return JSON.parse(localStorage.getItem(productCardViewKey) ?? true);
};
export const setCardViewToLocalStorage = (cardView) =>
  localStorage.setItem(productCardViewKey, cardView);
