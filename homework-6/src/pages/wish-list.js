import { useSelector } from "react-redux";
import ProductList from "../components/product-list/product-list";

export default function WishList(props) {
  const products = useSelector((state) =>
    state.products.products.filter(({ id }) => state.wishList.includes(id))
  );
  const showLoader = useSelector((state) => state.products.isFetching);
  return (
    <>
      <h1>Wish List</h1>
      {products.length === 0 && !showLoader && "No Items In Wish List"}
      <ProductList products={products} showLoader={showLoader} />
      {/* {!isProductsLoaded && <Loader />} */}
    </>
  );
}
