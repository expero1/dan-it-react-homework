import { useSelector } from "react-redux";
import ProductList from "../components/product-list/product-list";
import CheckoutForm from "../components/checkout-form/checkout-form";

export default function Cart(props) {
  const cart = useSelector((state) => state.cart);

  const products = useSelector((state) =>
    state.products.products.filter(({ id }) => cart.includes(id))
  );
  const showLoader = useSelector((state) => state.products.isFetching);

  const isNoProductsInCart = !showLoader && products.length === 0;
  const isShowCheckoutForm = !showLoader && products.length > 0;
  return (
    <>
      <h1>Cart</h1>
      {isNoProductsInCart && <h2>No Items in Cart</h2>}
      <>
        {isShowCheckoutForm && <CheckoutForm products={products} />}
        <ProductList
          products={products}
          showLoader={showLoader}
          closeButton={true}
        />
      </>
    </>
  );
}
