import React from "react";
import ReactDOM from "react-dom/client";
import { Provider as Redux } from "react-redux";
import "./index.css";
import App from "./App";
import { store } from "./redux/store";
import ProductListViewContext from "./context/context";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Redux store={store}>
    <ProductListViewContext>
      <App />
    </ProductListViewContext>
  </Redux>
);
