import React, { useEffect, useState } from "react";
import {
  getCardViewFromLocalStorage,
  setCardViewToLocalStorage,
} from "../services/local-storage";

export const ProductListViewContext = React.createContext();
const ProductListViewContextProvider = ({ children }) => {
  const [productListView, setProductListView] = useState(
    getCardViewFromLocalStorage()
  );
  useEffect(() => {
    setCardViewToLocalStorage(productListView);
  }, [productListView]);
  const toggleProductListView = () => {
    setProductListView(!productListView);
  };
  return (
    <ProductListViewContext.Provider
      value={{ productListView, toggleProductListView }}>
      {children}
    </ProductListViewContext.Provider>
  );
};
export default ProductListViewContextProvider;
