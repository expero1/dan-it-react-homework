import { screen, render, act } from "@testing-library/react";
import "@testing-library/jest-dom";
import { testSnapshot } from "../../test-helpers/test-helpers";
import CheckoutForm from "./checkout-form";
import formSettings from "./form-settings";
import { store } from "../../redux/store";
import { Provider } from "react-redux";
import userEvent from "@testing-library/user-event";
const formLabels = [];
Object.values(formSettings).forEach(({ title }) => {
  // console.log(title);
  title !== undefined && formLabels.push(title);
});
// console.log(formLabels);
const TestComponent = () => (
  <Provider store={store}>
    <CheckoutForm />
  </Provider>
);
// const formInputNames = ["firstName", "lastName", "tel", "age", "address"];
const formInputWrongValues = {
  firstName: "",
  lastName: "",
  tel: 1,
  age: -1,
  address: "",
};
describe("Checkout form", () => {
  testSnapshot("snapshot", <TestComponent />);
  test("component renders", () => {
    render(<TestComponent />);

    formLabels.forEach((label) => {
      expect(screen.getByLabelText(label)).toBeInTheDocument();
    });
    const submitButton = screen.getAllByRole("button").filter((button) => {
      return button.type === "submit";
    });
    const resetButton = screen.getAllByRole("button").filter((button) => {
      return button.type === "reset";
    });
    expect(submitButton.length).not.toBe(0);
    expect(resetButton.length).not.toBe(0);
    // expect(submitButton[0]).toBeInTheDocument();

    // expect(screen.getByText("r")).toBeInTheDocument();
  });
  test.skip("form error check", async () => {
    const { rerender } = render(<TestComponent />);
    act(async () => {
      // userEvent.type(screen.getByLabelText("First Name"), "");
      await userEvent.click(screen.getByLabelText("First Name"));
      await userEvent.click(screen.getByLabelText("Last Name"));
      await userEvent.tab();
      await userEvent.tab();
      await userEvent.tab();
      await userEvent.click(
        screen.getAllByRole("button").filter((button) => {
          return button.type === "submit";
        })[0]
      );
    });
    expect(await screen.findByRole("alert")).toBeInTheDocument();
    rerender(<TestComponent />);
    // screen.debug();
    // console.log(store.getState());
    // expect(screen.getByText("r")).toBeInTheDocument();
  });
});
