import { Field, ErrorMessage } from "formik";

const FormInput = ({
  name,
  title,
  component = "input",
  type = "text",
  className = "",
  placeholder,
  meta,
}) => (
  <>
    <label className="form-item">
      <span className="form-item__title">{title}</span>
      <Field
        name={name}
        component={component}
        type={type}
        className={`form-item__input ${className}`}
        placeholder={placeholder}
        {...meta}
      />
      <ErrorMessage
        className="form-error"
        component="div"
        name={name}
        meta={{ role: "alert" }}
      />
    </label>
  </>
);
export default FormInput;
