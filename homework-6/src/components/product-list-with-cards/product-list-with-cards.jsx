import ProductCard from "../product-card/product-card";

const ProductListWithCards = ({ products, closeButton = false }) => {
  return (
    <ul className="product-list">
      {products.map((product) => (
        <li className="product-card" key={product.productSKU}>
          <ProductCard productInfo={product} closeButton={closeButton} />
        </li>
      ))}
    </ul>
  );
};
export default ProductListWithCards;
