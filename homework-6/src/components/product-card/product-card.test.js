import { render, screen } from "@testing-library/react";
import { testSnapshot } from "../../test-helpers/test-helpers";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import ProductCard from "./product-card";
import { Provider } from "react-redux";
import { store } from "../../redux/store";
// const { default: ProductCard } = require("./product-card")
const productInfo = {
  id: 0,
  productName: "test product 1",
  productPrice: 10000,
  productSKU: "test-sku-1",
  productImage: "test-image.jpg",
  productColor: "black",
};

const cardComponent = (
  <Provider store={store}>
    <ProductCard productInfo={productInfo} />
  </Provider>
);
describe("Product card", () => {
  testSnapshot("has correct snapshot", cardComponent);
  test("show on page", () => {
    render(cardComponent);
    // screen.debug();
    expect(screen.getByText(productInfo.productName)).toBeInTheDocument();
    expect(screen.getByText(/10000/)).toBeInTheDocument();
    expect(screen.getByText(/test-sku-1/)).toBeInTheDocument();
    expect(screen.getByText(/black/)).toBeInTheDocument();
    expect(screen.getByAltText(productInfo.productName)).toBeInTheDocument();
  });
});
