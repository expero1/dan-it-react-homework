import { BuyButton } from "../buy-button/buy-button";
import { ProductCardWishList } from "../product-card-wish-list/product-card-wish-list";
import ProductInfo from "../product-info/product-info";
import { CloseButton } from "../close-button/close-button";
import "./product-card.scss";
import removeProductFromCartModal from "../../modal-content/remove-from-cart-modal";
import {
  useAddToCardCallback,
  useIsProductInCart,
  useIsProductInWishList,
  useToggleProductInWishListCallback,
} from "../../custom-hooks/custom-hooks";
export default function ProductCard({ productInfo, closeButton = false }) {
  const productId = productInfo.id;
  const isProductInCart = useIsProductInCart(productId);
  const isProductInWishList = useIsProductInWishList(productId);

  return (
    <>
      {closeButton && (
        <CloseButton
          className="product-card-close-btn"
          onClick={() => {
            removeProductFromCartModal(productId);
          }}
        />
      )}
      <ProductInfo {...productInfo} />
      <ProductCardWishList
        isProductInWishList={isProductInWishList}
        toggleWishListCallback={useToggleProductInWishListCallback(productId)}
      />
      <BuyButton
        addToCardCallback={useAddToCardCallback(productId)}
        isProductInCart={isProductInCart}
      />
    </>
  );
}
