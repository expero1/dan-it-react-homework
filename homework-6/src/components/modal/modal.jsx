import React from "react";
import "./modal.scss";
import { CloseButton } from "../close-button/close-button";
import { useSelector, useDispatch } from "react-redux";
import { hideModalAction } from "../../redux/action/modal-action";
function clickHandler(e, closeCallback) {
  if (
    e.target.classList.contains("modal") ||
    e.target.classList.contains("close-btn")
  ) {
    closeCallback();
  }
}

function renderActionButtonsSection(actionButtons) {
  return !actionButtons
    ? []
    : actionButtons.map((actionButton, index) => (
        <li key={index}>{actionButton}</li>
      ));
}
function Modal(props) {
  const modalContent = useSelector((state) => state.modal);
  const isModalOpen = Object.keys(modalContent).length > 0;
  const dispatch = useDispatch();
  const closeCallback = () => dispatch(hideModalAction());
  if (!isModalOpen) return null;
  const { header, text, closeButton, action } = modalContent;
  return (
    <div
      className="modal"
      role="dialog"
      onClick={(e) => {
        clickHandler(e, closeCallback);
      }}>
      <div className="modal__content">
        <div className="modal__header">
          {header}
          {closeButton && <CloseButton />}
        </div>
        <div className="modal__body">
          {typeof text === "string" ? text : { ...text }}
        </div>
        <ul className="modal-button__block">
          {[...renderActionButtonsSection(action)]}
        </ul>
      </div>
    </div>
  );
}

export default Modal;
// export default connect(mapStateToProps)(Modal);
