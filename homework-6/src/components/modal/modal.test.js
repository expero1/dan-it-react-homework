import { render, screen, act } from "@testing-library/react";

import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import Modal from "./modal";
import { Provider } from "react-redux";
import { store } from "../../redux/store";
import {
  hideModalAction,
  showModalAction,
} from "../../redux/action/modal-action";
import Button from "../button/button";
import { testSnapshot } from "../../test-helpers/test-helpers";
const mockFunction = jest.fn();
const mockButton = [<Button onClick={mockFunction} />];
const modalContent = {
  header: "test header",
  text: "test text",
  closeButton: true,
  action: [],
};
const modalComponent = (
  <Provider store={store}>
    <Modal />
  </Provider>
);
const showModal = (changedModalContent) =>
  act(() =>
    store.dispatch(showModalAction({ ...modalContent, ...changedModalContent }))
  );
const hideModal = () => act(() => store.dispatch(hideModalAction()));
const checkModalInDocument = () =>
  expect(screen.getByRole("dialog")).toBeInTheDocument();
const checkModalNotInDocument = () =>
  expect(screen.queryByRole("dialog")).not.toBeInTheDocument();
describe("Modal Window", () => {
  testSnapshot("renders correctly", modalComponent);
  test("present in document", () => {
    render(modalComponent);
    checkModalNotInDocument();
    showModal();
    checkModalInDocument();
    expect(screen.getByText(modalContent.header)).toBeInTheDocument();
    expect(screen.getByText(modalContent.text)).toBeInTheDocument();
  });

  test("closing when click outside window,", () => {
    showModal();
    render(modalComponent);
    expect(screen.getByRole("dialog")).toBeInTheDocument();
    // eslint-disable-next-line testing-library/no-unnecessary-act
    act(() => {
      userEvent.click(screen.getByRole("dialog"));
    });

    expect(screen.queryByRole("dialog")).not.toBeInTheDocument();
  });
});

test("closeButton visible", () => {
  render(modalComponent);
  showModal({ closeButton: false });
  expect(screen.queryByRole("button")).not.toBeInTheDocument();
  hideModal();
  showModal({ closeButton: true });
  expect(screen.getByRole("button")).toBeInTheDocument();
});

test("closing if click on closeButton", async () => {
  render(modalComponent);
  showModal({ closeButton: true });
  const button = screen.getByRole("button");
  // eslint-disable-next-line testing-library/no-unnecessary-act
  act(() => userEvent.click(button));
  checkModalNotInDocument();
});

test("Calls action buttons callback", () => {
  render(modalComponent);
  showModal({ action: mockButton, closeButton: false });
  const actionButtons = screen.getAllByRole("button");
  expect(actionButtons.length).toBe(1);
  userEvent.click(actionButtons[0]);
  expect(mockFunction.mock.calls.length).toBe(1);
});
