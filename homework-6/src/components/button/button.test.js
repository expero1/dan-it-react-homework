import Button from "./button";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";

describe("Check Button component", () => {
  test("Check Button in document", () => {
    const cbReturnValue = "test fn";
    const cb = jest.fn(() => cbReturnValue);
    render(
      <Button
        backgroundColor="red"
        text="Test Button"
        dataSet={{ test: "test1" }}
        type={"subscribe"}
        // disabled={true}
        onClick={cb}
        meta={{ test2: "test3" }}
      />
    );
    const button = screen.getByRole("button");
    userEvent.click(button);
    expect(button).toBeInTheDocument();
    expect(cb).toBeCalledTimes(1);
    expect(cb.mock.results[0].value).toBe(cbReturnValue);
  });
});
