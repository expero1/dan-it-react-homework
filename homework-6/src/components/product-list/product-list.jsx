import React, { useContext } from "react";
import "./product-list.scss";
import Loader from "../loader/loader";
import { ProductListViewContext } from "../../context/context";
import Button from "../button/button";

import ProductListWithCards from "../product-list-with-cards/product-list-with-cards";
import ProductTable from "../product-table/product-table";
export default function ProductList({
  products = [],
  showLoader,
  closeButton = false,
}) {
  const { productListView, toggleProductListView } = useContext(
    ProductListViewContext
  );

  return (
    <section>
      <Button
        backgroundColor={"red"}
        text="change view"
        onClick={toggleProductListView}
      />
      {productListView === true ? (
        <ProductListWithCards products={products} closeButton={closeButton} />
      ) : (
        <ProductTable products={products} closeButton={closeButton} />
      )}

      {showLoader && <Loader />}
    </section>
  );
}
