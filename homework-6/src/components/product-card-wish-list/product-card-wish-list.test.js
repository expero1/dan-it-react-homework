import {
  screen,
  render,
  act,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import { store } from "../../redux/store";
import { Provider, useSelector, useDispatch } from "react-redux";
import { ProductCardWishList } from "./product-card-wish-list";
import { toggleProductInWishListAction } from "../../redux/action/wish-list-action";
import "@testing-library/jest-dom";
import userEvent from "@testing-library/user-event";
import { testSnapshot } from "../../test-helpers/test-helpers";
let productId = 0;
const productNotInWishListText = "Add to wish list";
const productInWishListText = "Added to wish list";
const productNotInWishListImgAlt = "add to wish list";
const productInWishListImgAlt = "product in wish list";
// const isProductinWishList = () => store.getState().wishList.includes(productId);
// const toggleWishListCallback = () => {
//   console.log("click");
//   store.dispatch(toggleProductInWishListAction(productId));
// };
const TestComponent = ({ productId }) => {
  const isProductInWishList = useSelector((state) =>
    state.wishList.includes(productId)
  );
  const dispatch = useDispatch();
  const toggleWishListCallback = () => {
    dispatch(toggleProductInWishListAction(productId));
  };
  return (
    <ProductCardWishList
      isProductInWishList={isProductInWishList}
      toggleWishListCallback={toggleWishListCallback}
    />
  );
};
describe("ProductCardWishList", () => {
  testSnapshot("shapshot", <ProductCardWishList />);
  test("component renders", async () => {
    // console.log(isProductnWishList());
    render(
      <Provider store={store}>
        <TestComponent productId={productId} />
      </Provider>
    );

    // let wishListButton = screen.getByText(productNotInWishListText);
    let wishListButton = screen.getByRole("button");
    let wishListButtonImg = screen.getByAltText(productNotInWishListImgAlt);
    expect(wishListButton).toBeInTheDocument();
    expect(wishListButtonImg).toBeInTheDocument();
    act(() => userEvent.click(wishListButton));
    // userEvent.click(wishListButton);
    // await waitForElementToBeRemoved(() => wishListButtonImg);
    // await waitForElementToBeRemoved(() =>
    //   screen.queryByText(productInWishListText)
    // );

    // userEvent.click(wishListButton);

    wishListButton = screen.getByText(productInWishListText);
    wishListButtonImg = screen.getByAltText(productInWishListImgAlt);
    expect(wishListButton).toBeInTheDocument();
    expect(wishListButtonImg).toBeInTheDocument();
    // screen.debug();
  });
});
