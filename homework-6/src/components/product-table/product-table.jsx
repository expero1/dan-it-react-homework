import { ProductTableRow } from "./product-row";
import "./product-table.scss";

const ProductTable = ({ products, closeButton = false }) => {
  if (products.length === 0) return null;

  return (
    <table className="product-table">
      <thead>
        <tr>
          <th>Image</th>
          <th>SKU</th>
          <th>Name</th>
          <th>Price</th>
          <th>Color</th>
        </tr>
      </thead>
      <tbody>
        {products.map((product) => (
          <ProductTableRow
            key={product.productSKU}
            product={product}
            closeButton={closeButton}
          />
        ))}
      </tbody>
    </table>
  );
};

export default ProductTable;
