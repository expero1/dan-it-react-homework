import {
  useAddToCardCallback,
  useIsProductInCart,
  useIsProductInWishList,
  useToggleProductInWishListCallback,
} from "../../custom-hooks/custom-hooks";
import { BuyButton } from "../buy-button/buy-button";
import { ProductCardWishList } from "../product-card-wish-list/product-card-wish-list";
import { CloseButton } from "../close-button/close-button";
import removeProductFromCartModal from "../../modal-content/remove-from-cart-modal";

export const ProductTableRow = ({ product, closeButton = false }) => {
  const {
    id: productId,
    productSKU,
    productName,
    productImage = "https://placehold.co/600x400?text=No+Image",
    productPrice: price,
    productColor: color,
  } = product;
  return (
    <>
      <tr>
        <td className="product-table__image">
          <img src={productImage} alt={productName}></img>
        </td>
        <td className="product-table__sku">{productSKU}</td>
        <td className="product-table__name">{productName}</td>

        <td className="product-table__price">{price}</td>
        <td className="product-table__color">{color}</td>

        <td>
          <span className="product-table__actions">
            <BuyButton
              addToCardCallback={useAddToCardCallback(productId)}
              isProductInCart={useIsProductInCart(productId)}
            />
            <ProductCardWishList
              isProductInWishList={useIsProductInWishList(productId)}
              toggleWishListCallback={useToggleProductInWishListCallback(
                productId
              )}
            />
          </span>
        </td>

        {closeButton && (
          <td className="product-table__remove">
            <CloseButton
              className="product-table__remove-btn"
              onClick={() => {
                removeProductFromCartModal(productId);
              }}
            />
          </td>
        )}
      </tr>
    </>
  );
};
