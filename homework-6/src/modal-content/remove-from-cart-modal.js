import Button from "../components/button/button";
import { hideModalAction, showModalAction } from "../redux/action/modal-action";
import { removeFromCartAction } from "../redux/action/cart-action";
import { store } from "../redux/store";
const dispatch = store.dispatch;

const showModal = (modalContent) => {
  dispatch(showModalAction(modalContent));
};
const removeFromCart = (productId) => {
  dispatch(removeFromCartAction(productId));
};
const hideModal = () => {
  dispatch(hideModalAction());
};
export default function removeProductFromCartModal(id) {
  showModal({
    header: "Do You want to delete product from cart?",
    text: "Please Choose",
    closeButton: true,
    closeCallback: hideModal,
    action: [
      <Button
        backgroundColor="red"
        text="Remove From Cart"
        onClick={() => {
          removeFromCart(id);
          hideModal();
        }}
      />,
      <Button backgroundColor="grey" text="cancel" onClick={hideModal} />,
    ],
  });
}
