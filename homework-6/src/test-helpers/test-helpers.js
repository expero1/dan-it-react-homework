import renderer from "react-test-renderer";

export const testSnapshot = (message, component) => {
  test(message, () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
};
