import { useDispatch, useSelector } from "react-redux";
import { toggleProductInWishListAction } from "../redux/action/wish-list-action";
import addToCardModal from "../modal-content/add-to-card-modal";
export const useIsProductInWishList = (productId) => {
  return useSelector((state) => state.wishList.includes(productId));
};
export const useIsProductInCart = (productId) =>
  useSelector((state) => state.cart.includes(productId));
export const useToggleProductInWishListCallback = (productId) => {
  const dispatch = useDispatch();
  return () => dispatch(toggleProductInWishListAction(productId));
};
export const useAddToCardCallback = (productId) => () => {
  addToCardModal(productId);
};
