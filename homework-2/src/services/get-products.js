export const getProducts = () => {
  const responseTimeout = 1500;
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(
        fetch("/product-data.json").then((response) =>
          response.text().then((result) => JSON.parse(result))
        )
      );
    }, responseTimeout);
  });
};
