import React from "react";
import "./button.scss";
export class Button extends React.Component {
  /*backgroundColor, text, onClick, dataSet:Object*/
  renderDataAttributes(dataSet) {
    const renderedAttributes = {};
    for (let attribute in dataSet) {
      renderedAttributes[`data-${attribute}`] = dataSet[attribute];
    }
    return renderedAttributes;
  }
  render() {
    this.renderDataAttributes(this.props.dataSet);
    return (
      <button
        className="btn"
        style={{ backgroundColor: this.props.backgroundColor }}
        onClick={this.props.onClick}
        {...this.renderDataAttributes(this.props.dataSet)}>
        {this.props.text}
      </button>
    );
  }
}
