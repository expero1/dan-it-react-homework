import React from "react";
import PropTypes from "prop-types";
import { Button } from "../button/Button";
import "./product-card.scss";
import { ProductCardWishList } from "../product-card-wish-list/product-card-wish-list";

class ProductCard extends React.Component {
  render() {
    return (
      <div className="product-card">
        <div className="product-card__image">
          <img src={this.props.productImage} alt={this.props.productName} />
        </div>
        <h2 className="product-card__name">{this.props.productName}</h2>
        <span className="product-card__price">
          Price: {this.props.productPrice} UAH
        </span>

        <span className="product-card__sku">SKU: {this.props.productSKU}</span>
        {this.props.productColor && (
          <span className="product-card__color">
            Color: {this.props.productColor}
          </span>
        )}
        <ProductCardWishList
          id={this.props.id}
          isProductInWishList={this.props.isProductInWishList}
          toggleWishListCallback={this.props.toggleWishListCallback}
        />
        <Button
          backgroundColor="red"
          text={`Buy`}
          onClick={() => {
            this.props.addToCartCallback(this.props.id);
          }}
        />
      </div>
    );
  }
}
ProductCard.propTypes = {
  productId: PropTypes.number,
  productImage: PropTypes.string,
  productPrice: PropTypes.number,
  productSKU: PropTypes.string,
  productColor: PropTypes.string,
};
ProductCard.defaultProps = {
  productImage: "https://placehold.co/600x400?text=No+Image",
  productColor: "",
};
export { ProductCard };
