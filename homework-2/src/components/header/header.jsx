import React from "react";
import "./header.scss";
import shoppingCart from "../../image/cart-shopping-solid.svg";
import starSolid from "../../image/star-solid.svg";
import logo from "../../image/font-awesome-regular.svg";
export class Header extends React.Component {
  //cardItems, wishItems
  render() {
    return (
      <section className="header">
        <div className="logo">
          <img src={logo} alt="site logo" />
        </div>
        <div className="cart">
          <img src={shoppingCart} alt="Shopping Cart" />
          Cart:({this.props.cartItems ?? 0})
        </div>
        <div className="wish-list">
          <img src={starSolid} alt="Wish List" />
          Wish List: ({this.props.wishListCount ?? 0})
        </div>
      </section>
    );
  }
}
