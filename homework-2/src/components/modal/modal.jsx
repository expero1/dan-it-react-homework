import React from "react";
import "./modal.scss";
export class Modal extends React.Component {
  constructor(props) {
    //header, text, closeButton, actions
    super(props);
    this.clickHandler = this.clickHandler.bind(this);
  }

  clickHandler(e) {
    if (
      e.target.classList.contains("modal") ||
      e.target.classList.contains("close-btn")
    ) {
      this.props.closeCallback();
    }
  }
  render() {
    return Object.keys(this.props).length > 0
      ? this.renderModalContent()
      : null;
  }
  renderActionButtonsSection(actionButtons) {
    return !actionButtons
      ? []
      : actionButtons.map((actionButton, index) => (
          <li key={index}>{actionButton}</li>
        ));
  }

  renderModalContent() {
    return (
      <div className="modal" onClick={(e) => this.clickHandler(e)}>
        <div className="modal__content">
          <div className="modal__header">
            {this.props.header}
            {this.props.closeButton && <button className="close-btn">X</button>}
          </div>
          <div className="modal__body">{this.props.text}</div>
          <ul className="modal-button__block">
            {[...this.renderActionButtonsSection(this.props.action)]}
          </ul>
        </div>
      </div>
    );
  }
}
