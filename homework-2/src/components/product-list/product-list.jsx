import React from "react";
import { ProductCard } from "../product-card/product-card";
import "./product-list.scss";
export class ProductList extends React.Component {
  render() {
    return (
      <section className="product-list">
        {this.props.productInfoLoaded
          ? this.props.productsInfo.map((product) => (
              <ProductCard
                {...product}
                addToCartCallback={this.props.addToCartCallback}
                toggleWishListCallback={this.props.toggleWishListCallback}
                isProductInWishList={this.props.wishList.includes(product.id)}
                key={product.productSKU}
              />
            ))
          : "LOADING..."}
      </section>
    );
  }
}
