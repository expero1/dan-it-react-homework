import "./App.css";
import { Modal } from "./components/modal/modal";
import { Header } from "./components/header/header";
import { ProductList } from "./components/product-list/product-list";
import React from "react";
import {
  getWishListFromLocalStorage,
  setWishListToLocalStarage,
  getCartFromLocalStorage,
  setCartToLocalStorage,
} from "./services/local-storage";
import { getProducts } from "./services/get-products";
import { Button } from "./components/button/Button";
export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cart: [],
      wishList: [],
      productInfo: [],
      productInfoLoaded: false,
      modalContent: {},
    };
    this.addToCart = this.addToCart.bind(this);
    this.addToCartModal = this.addToCartModal.bind(this);
    this.toggleProductInWishList = this.toggleProductInWishList.bind(this);
    this.getProductInfo = this.getProductInfo.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  saveState = (state) => {
    console.log("Previous state", this.state);
    console.log("Current changes", state);
    const logAfterStateUpdated = () => {
      console.log("Next state", this.state);
    };
    this.setState(state, logAfterStateUpdated);
  };
  componentDidMount() {
    this.getProductInfo();
    const cart = getCartFromLocalStorage();
    this.saveState({
      cart: cart,
      wishList: getWishListFromLocalStorage(),
    });
    console.log(this.state);
  }
  componentDidUpdate() {
    setCartToLocalStorage(this.state.cart);
    setWishListToLocalStarage(this.state.wishList);
    // console.log("update");
  }
  async getProductInfo() {
    const productInfo = await getProducts();
    this.saveState({ productInfo, productInfoLoaded: true });
  }
  toggleProductInWishList(productId) {
    const wishListSet = new Set(this.state.wishList);
    wishListSet.has(productId)
      ? wishListSet.delete(productId)
      : wishListSet.add(productId);
    this.saveState({ wishList: Array.from(wishListSet) });
  }
  addToCartModal(productInfo) {
    this.saveState({
      modalContent: {
        header: "Do You want to add product to Basket",
        text: "Please Choose",
        closeButton: true,
        closeCallback: this.closeModal,
        action: [
          <Button
            backgroundColor="green"
            text="add"
            onClick={() => {
              this.addToCart(productInfo);
              this.closeModal();
            }}
          />,
          <Button
            backgroundColor="grey"
            text="cancel"
            onClick={this.closeModal}
          />,
        ],
      },
    });
  }

  closeModal() {
    this.saveState({ modalContent: {} });
  }
  addToCart(productId) {
    // console.log("add to cart " + productId);
    const cart = new Set(this.state.cart);
    cart.add(productId);
    this.saveState({ cart: Array.from(cart) });
  }

  render() {
    return (
      <>
        <Modal {...this.state.modalContent} />
        <Header
          cartItems={this.state.cart.length}
          wishListCount={this.state.wishList.length}
        />

        <ProductList
          productsInfo={this.state.productInfo}
          productInfoLoaded={this.state.productInfoLoaded}
          addToCartCallback={this.addToCartModal}
          toggleWishListCallback={this.toggleProductInWishList}
          wishList={this.state.wishList}
        />
      </>
    );
  }
}
