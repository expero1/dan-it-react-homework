import "./close-button.scss";
export function CloseButton({ className = "", onClick = () => {} }) {
  return (
    <button className={`close-btn ${className}`} onClick={onClick}></button>
  );
}
