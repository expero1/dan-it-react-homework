import React from "react";
import "./modal.scss";
import { CloseButton } from "../close-button/close-button";
import { connect } from "react-redux";
function clickHandler(e, closeCallback) {
  if (
    e.target.classList.contains("modal") ||
    e.target.classList.contains("close-btn")
  ) {
    closeCallback();
  }
}

function renderActionButtonsSection(actionButtons) {
  return !actionButtons
    ? []
    : actionButtons.map((actionButton, index) => (
        <li key={index}>{actionButton}</li>
      ));
}
function Modal(props) {
  if (!props.isModalOpen) return null;
  const { header, text, closeButton, action, closeCallback } =
    props.modalContent;
  return (
    <div
      className="modal"
      onClick={(e) => {
        clickHandler(e, closeCallback);
      }}>
      <div className="modal__content">
        <div className="modal__header">
          {header}
          {closeButton && <CloseButton />}
        </div>
        <div className="modal__body">
          {typeof text === "string" ? text : { ...text }}
        </div>
        <ul className="modal-button__block">
          {[...renderActionButtonsSection(action)]}
        </ul>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    modalContent: state.modal,
    isModalOpen: Object.keys(state.modal).length > 0,
  };
};

export default connect(mapStateToProps)(Modal);
