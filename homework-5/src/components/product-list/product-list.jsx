import React from "react";
import "./product-list.scss";
import ProductCard from "../product-card/product-card";
import Loader from "../loader/loader";

export default function ProductList({
  products = [],
  showLoader,
  closeButton = false,
}) {
  // console.log(products);
  return (
    <section>
      <ul className="product-list">
        {products.map((product) => (
          <li className="product-card" key={product.productSKU}>
            <ProductCard productInfo={product} closeButton={closeButton} />
          </li>
        ))}
      </ul>
      {showLoader && <Loader />}
    </section>
  );
}
