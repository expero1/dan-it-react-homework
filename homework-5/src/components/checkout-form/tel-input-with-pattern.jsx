import { PatternFormat } from "react-number-format";

export const TelInputWithPattern = ({ field }) => {
  return (
    <PatternFormat
      {...field}
      className="form-item__input"
      format="(###)###-##-##"
      allowEmptyFormatting
      mask="_"
    />
  );
};
export default TelInputWithPattern;
