export const formSettings = {
  firstName: {
    name: "firstName",
    title: "First Name",
    placeholder: "Please Enter First Name",
  },
  lastName: {
    name: "lastName",
    title: "Last Name",
    placeholder: "Please Enter Last Name",
  },
  age: {
    name: "age",
    title: "Age",
    placeholder: "Please Enter Your Age",
    type: "number",
    meta: { min: 1 },
  },
  address: {
    name: "address",
    title: "Shipping Address",
    placeholder: "Please enter Shipping Address",
  },
  tel: {
    name: "tel",
    title: "Telefone number",
  },
  submit: {
    backgroundColor: "red",
    text: "Checkout",
    type: "submit",
  },
  reset: {
    backgroundColor: "grey",
    text: "Reset",
    type: "reset",
  },
};
export default formSettings;
