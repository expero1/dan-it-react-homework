import "./checkout-form.scss";
import { Form, Formik } from "formik";
import { useDispatch } from "react-redux";
import * as Yup from "yup";
import Button from "../button/button";
import { checkoutAction } from "../../redux/action/cart-action";
import formSettings from "./form-settings";
import FormInput from "./form-input";
import TelInputWithPattern from "./tel-input-with-pattern";
const requiredMessage = "Required Field";
const checkTelNumberMessage = "Check Telephone Number";
const normalizeTelNumber = (rawTelNumber = "") =>
  rawTelNumber.replaceAll(/[\D]/g, "");

const validationSchema = () =>
  Yup.object({
    firstName: Yup.string().required(requiredMessage),
    lastName: Yup.string().required(requiredMessage),
    age: Yup.number().required(requiredMessage),
    tel: Yup.string()
      .test(
        "is-tel-number",
        () => checkTelNumberMessage,
        (number) => {
          const normalizedNumber = normalizeTelNumber(number);
          return normalizedNumber.length === 10;
        }
      )
      .required(requiredMessage),
    address: Yup.string().required(requiredMessage),
  });
const formikSettings = {
  initialValues: {
    firstName: "",
    lastName: "",
    tel: "",
    age: "",
    address: "",
  },
  validationSchema: validationSchema,
};
export default function CheckoutForm() {
  const dispatch = useDispatch();
  return (
    <>
      <Formik
        {...formikSettings}
        onSubmit={(shippingInformation) => {
          dispatch(checkoutAction(shippingInformation));
        }}>
        {({ values, errors, isValid }) => {
          return (
            <Form className="checkout-form">
              <h2>Checkout Form</h2>
              <FormInput {...formSettings.firstName} />
              <FormInput {...formSettings.lastName} />
              <FormInput {...formSettings.age} />
              <FormInput
                {...formSettings.address}
                className="form__text-area"
                component="textarea"
              />
              <FormInput
                {...formSettings.tel}
                component={TelInputWithPattern}
              />
              <Button {...formSettings.submit} />
              <Button {...formSettings.reset} />
              {!isValid && console.log(values, errors)}
            </Form>
          );
        }}
      </Formik>
    </>
  );
}
