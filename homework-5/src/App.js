import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Cart from "./pages/cart";
import WishList from "./pages/wish-list";
import "./App.css";
import Modal from "./components/modal/modal.jsx";
import { Header } from "./components/header/header";
import { NoPage } from "./components/no-page/no-page";
import Products from "./pages/products";

export default function App(props) {
  return (
    <>
      <Modal />
      <BrowserRouter>
        <Header
          cardItems={props.productQuantity}
          wishListCount={props.productInWishListQuantity}
        />
        <Routes>
          <Route path="/" element={<Products />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/wish-list" element={<WishList />} />
          <Route path="*" element={<NoPage />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}
