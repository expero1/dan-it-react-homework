export const TOGGLE_PRODUCT_IN_WISH_LIST = "TOGGLE-PRODUCT-IN-WISHLIST";

export const toggleProductInWishListAction = (productId) => {
  return {
    type: TOGGLE_PRODUCT_IN_WISH_LIST,
    payload: productId,
  };
};
