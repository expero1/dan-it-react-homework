export const ADD_TO_CART = "ADD-TO-CART";
export const REMOVE_FROM_CART = "REMOVE-FROM-CART";
export const CLEAR_CART = "CLEAR-CART";
export const CHECKOUT = "CHECKOUT";

export const addToCartAction = (productId, price) => {
  return {
    type: ADD_TO_CART,
    payload: productId,
  };
};
export const removeFromCartAction = (productId) => {
  return {
    type: REMOVE_FROM_CART,
    payload: productId,
  };
};
export const clearCartAction = () => {
  return {
    type: CLEAR_CART,
  };
};
export const checkoutAction = (shippingInformation) => {
  return function (dispatch, getState) {
    console.log(getState());
    const productsInCart = getState().cart;
    const productInformation = getState().products.products.filter((product) =>
      productsInCart.includes(product.id)
    );
    console.log("shipping-info: ", JSON.stringify(shippingInformation));
    console.log("products-info", JSON.stringify(productInformation));
    dispatch(clearCartAction());
  };
};
