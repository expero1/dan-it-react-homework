import {
  ADD_TO_CART,
  CLEAR_CART,
  REMOVE_FROM_CART,
} from "../action/cart-action";
import { getCartFromLocalStorage } from "../../services/local-storage";

const addToCard = (currentCart, productId) => {
  const currentCartSet = new Set(currentCart);
  return Array.from(currentCartSet.add(productId));
};

const removeFromCart = (currentCart, productId) => {
  return currentCart.filter((id) => id !== productId);
};

const cartInitialState = [...getCartFromLocalStorage()];

const cartReducer = (state = cartInitialState, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      return addToCard(state, action.payload);
    case REMOVE_FROM_CART:
      return removeFromCart(state, action.payload);
    case CLEAR_CART:
      return [];

    default:
      return state;
  }
};
export default cartReducer;
