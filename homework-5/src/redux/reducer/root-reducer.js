import { combineReducers } from "redux";
import productReducer from "./fetch-product-reducer";
import modalReducer from "./modal-reducer";
import cartReducer from "./cart-reducer";
import wishListReducer from "./wish-list-reducer";
/* REDUCERS */

export const rootReducer = combineReducers({
  products: productReducer,
  modal: modalReducer,
  cart: cartReducer,
  wishList: wishListReducer,
});
