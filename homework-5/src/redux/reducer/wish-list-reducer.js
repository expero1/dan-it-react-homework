import { TOGGLE_PRODUCT_IN_WISH_LIST } from "../action/wish-list-action";
import { getWishListFromLocalStorage } from "../../services/local-storage";

const toggleProductInWishList = (currentWishList, productId) => {
  const wishListSet = new Set(currentWishList);
  wishListSet.has(productId)
    ? wishListSet.delete(productId)
    : wishListSet.add(productId);
  return Array.from(wishListSet);
};

const wishListInitialState = [...getWishListFromLocalStorage()];

const wishListReducer = (state = wishListInitialState, action) => {
  switch (action.type) {
    case TOGGLE_PRODUCT_IN_WISH_LIST:
      return toggleProductInWishList(state, action.payload);
    default:
      return state;
  }
};

export default wishListReducer;
