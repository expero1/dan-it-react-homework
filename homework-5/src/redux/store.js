import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
import { rootReducer } from "./reducer/root-reducer";
import {
  setCartToLocalStorage,
  setWishListToLocalStarage,
} from "../services/local-storage";
import { fetchProductsAction } from "./action/fetch-products-action";

export const store = createStore(rootReducer, applyMiddleware(thunk, logger));

store.subscribe(() => {
  setCartToLocalStorage(store.getState().cart);
});
store.subscribe(() => {
  setWishListToLocalStarage(store.getState().wishList);
});
store.dispatch(fetchProductsAction());
