// import { store } from "../redux/store";

// const basketKey = "basket";
// const wishListKey = "wish-list";

// // const createProductBasket = ({
// //   id = undefined,
// //   quantity = 0,
// //   price = undefined,
// // } = {}) => ({
// //   id,
// //   quantity,
// //   price,
// // });

// export const getProductBasket = () =>
//   JSON.parse(localStorage.getItem(basketKey) ?? "[]");

// // export const getProductsQuantityInBasket = () =>
// //   getProductBasket().filter(({ id }) => id !== undefined).length;
// export const saveCartToStorage = (cart) => {
//   localStorage.setItem(basketKey, JSON.stringify(cart));
// };
// export const saveWishListToStorage = (wishList) => {
//   localStorage.setItem(wishListKey, JSON.stringify(wishList));
// };
// // export const setProductToBasket = ({ id, productPrice }) => {
// //   let basketStorage = getProductBasket();
// //   let currentProductBasketIndex = basketStorage.findIndex(
// //     (productInBasket) => productInBasket.id === id
// //   );
// //   let currentProductBasket;
// //   if (currentProductBasketIndex === -1) {
// //     currentProductBasket = createProductBasket({
// //       id,
// //       price: productPrice,
// //     });
// //     basketStorage.push(currentProductBasket);
// //     currentProductBasketIndex = basketStorage.length - 1;
// //   } else {
// //     currentProductBasket = basketStorage[currentProductBasketIndex];
// //   }

// //   basketStorage[currentProductBasketIndex] = createProductBasket({
// //     ...currentProductBasket,
// //     quantity: ++currentProductBasket.quantity,
// //   });

// //   localStorage.setItem(basketKey, JSON.stringify(basketStorage));
// // };
// // const clearProductBasket = () => {
// //   localStorage.removeItem(basketKey);
// // };
// // export function removeProductFromBasket(productId) {
// //   const productBasket = getProductBasket();
// //   clearProductBasket();
// //   productBasket.forEach((product) => {
// //     if (product.id !== productId) setProductToBasket(product);
// //   });
// // }
// export const getWishList = () =>
//   JSON.parse(localStorage.getItem(wishListKey) ?? "[]");

// // export const toggleProductInWishList = (productId) => {
// //   const wishList = getWishList();
// //   wishList.has(productId)
// //     ? wishList.delete(productId)
// //     : wishList.add(productId);
// //   localStorage.setItem(wishListKey, JSON.stringify(Array.from(wishList)));
// // };

const cartKey = "basket";
const wishListKey = "wish-list";

const getValueByKeyFromLocalStorage = (key) => {
  const savedValue = localStorage.getItem(key);
  try {
    return savedValue ? Array.from(JSON.parse(savedValue)) : [];
  } catch (error) {
    console.log(error);
    return [];
  }
};
const saveToLocalStorage = (key, value) => {
  localStorage.setItem(key, JSON.stringify(value));
};
export const getCartFromLocalStorage = () =>
  getValueByKeyFromLocalStorage(cartKey).filter(
    (productId) => typeof productId === "number"
  );
export const setCartToLocalStorage = (cart) => {
  saveToLocalStorage(cartKey, cart);
};

export const getWishListFromLocalStorage = () =>
  getValueByKeyFromLocalStorage(wishListKey).filter(
    (productId) => typeof productId === "number"
  );
export const setWishListToLocalStarage = (wishList) => {
  saveToLocalStorage(wishListKey, wishList);
};
