import Button from "../components/button/button";
import { showModalAction, hideModalAction } from "../redux/action/modal-action";
import { addToCartAction } from "../redux/action/cart-action";
import { store } from "../redux/store";
const dispatch = store.dispatch;
function showModal(modalContent) {
  dispatch(showModalAction(modalContent));
}
function hideModal() {
  dispatch(hideModalAction());
}
function addToCard(productId) {
  dispatch(addToCartAction(productId));
}

export default function addToCardModal(productId) {
  showModal({
    header: "Do You want to add product to Basket",
    text: "Please Choose",
    closeButton: true,
    closeCallback: hideModal,
    action: [
      <Button
        backgroundColor="green"
        text="add"
        onClick={() => {
          addToCard(productId);
          hideModal();
        }}
      />,
      <Button backgroundColor="grey" text="cancel" onClick={hideModal} />,
    ],
  });
}
