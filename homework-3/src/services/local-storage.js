const cartKey = "cart";
const wishListKey = "wish-list";

const getValueByKeyFromLocalStorage = (key) => {
  const savedValue = localStorage.getItem(key);
  try {
    return savedValue ? Array.from(JSON.parse(savedValue)) : [];
  } catch (error) {
    console.log(error);
    return [];
  }
};
const saveToLocalStorage = (key, value) => {
  localStorage.setItem(key, JSON.stringify(value));
};
export const getCartFromLocalStorage = () =>
  getValueByKeyFromLocalStorage(cartKey).filter(
    (productId) => typeof productId === "number"
  );
export const setCartToLocalStorage = (cart) => {
  saveToLocalStorage(cartKey, cart);
};

export const getWishListFromLocalStorage = () =>
  getValueByKeyFromLocalStorage(wishListKey).filter(
    (productId) => typeof productId === "number"
  );
export const setWishListToLocalStarage = (wishList) => {
  saveToLocalStorage(wishListKey, wishList);
};
