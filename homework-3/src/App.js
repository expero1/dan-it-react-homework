import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Products from "./pages/products";
import Cart from "./pages/cart";
import WishList from "./pages/wish-list";
import "./App.css";
import { Modal } from "./components/modal/modal.jsx";
import { Header } from "./components/header/header";
import {
  getCartFromLocalStorage,
  setCartToLocalStorage,
  getWishListFromLocalStorage,
  setWishListToLocalStarage,
} from "./services/local-storage";
import { getProducts } from "./services/get-products";
import { Button } from "./components/button/button";
import { useState } from "react";
import { useEffect } from "react";
import { NoPage } from "./components/no-page/no-page";

function filterProducts(productInfo, productsInCart) {
  return productInfo.filter(({ id }) => productsInCart.includes(id));
}
export default function App(props) {
  const [cart, setCart] = useState(getCartFromLocalStorage());
  const [wishList, setWishList] = useState(getWishListFromLocalStorage());
  const [products, setProducts] = useState([]);
  const [modalContent, setModalContent] = useState({});
  const [productInfoLoaded, setProductInfoLoaded] = useState(false);

  useEffect(() => {
    getProducts().then((productInfo) => {
      setProducts(productInfo);
      setProductInfoLoaded(true);
    });
  }, []);
  useEffect(() => {
    setWishListToLocalStarage(wishList);
    // console.log("wishlist");
  }, [wishList]);
  useEffect(() => {
    // console.log("cart");
    setCartToLocalStorage(cart);
  }, [cart]);

  function closeModal() {
    setModalContent({});
  }
  function productSet(products) {
    return {
      products,
      productInfoLoaded,
      toggleWishListCallback: toggleWishList,
      addToCartCallback: addToCartModal,
      removeFromCartCallback: removeProductFromCartModal,
      wishList,
      cart,
    };
  }
  const state = {
    products,
    cart,
    wishList,
    setCart,
    setWishList,
  };
  function addToCart(productId) {
    const cartSet = new Set(cart);
    cartSet.add(productId);
    setCart(Array.from(cartSet));
  }
  function removeFromCart(productId) {
    const cartSet = new Set(cart);
    cartSet.delete(productId);
    setCart(Array.from(cartSet));
  }
  function toggleWishList(productId) {
    const wishListSet = new Set(wishList);
    wishListSet.has(productId)
      ? wishListSet.delete(productId)
      : wishListSet.add(productId);
    setWishList(Array.from(wishListSet));
  }

  function addToCartModal(productId) {
    setModalContent({
      header: "Do You want to add product to Basket",
      text: "Please Choose",
      closeButton: true,
      closeCallback: closeModal,
      action: [
        <Button
          backgroundColor="green"
          text="add"
          onClick={() => {
            addToCart(productId);
            closeModal();
          }}
        />,
        <Button backgroundColor="grey" text="cancel" onClick={closeModal} />,
      ],
    });
  }
  function removeProductFromCartModal(productId) {
    setModalContent({
      header: "Do You want to delete product from cart?",
      text: "Please Choose",
      closeButton: true,
      closeCallback: closeModal,
      action: [
        <Button
          backgroundColor="red"
          text="Remove From Cart"
          onClick={() => {
            removeFromCart(productId);
            closeModal();
          }}
        />,
        <Button backgroundColor="grey" text="cancel" onClick={closeModal} />,
      ],
    });
  }
  return (
    <>
      {Object.keys(modalContent).length > 0 && <Modal {...modalContent} />}
      <BrowserRouter>
        <Header cartQuantity={cart.length} wishListQuantity={wishList.length} />
        <Routes>
          <Route
            path="/"
            element={
              <Products
                // products={products}
                // productInfoLoaded={productInfoLoaded}
                // addToCartCallback={addToCartModal}
                // removeFromCartCallback={removeProductFromCartModal}
                // toggleWishListCallback={toggleWishList}
                // wishList={wishList}
                // cart={cart}
                {...productSet(products)}
              />
            }
          />
          <Route
            path="/cart"
            element={
              <Cart
                {...productSet(filterProducts(products, cart))}
                closeButton={true}
              />
            }
          />
          <Route
            path="/wish-list"
            element={
              <WishList {...productSet(filterProducts(products, wishList))} />
            }
          />
          <Route path="*" element={<NoPage />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}
