// import { ProductListWithCloseButton } from "../components/product-list-with-close-button/product-list-with-close-button";
import { ProductList } from "../components/product-list/product-list";

export default function Cart({
  products,
  productInfoLoaded,
  removeFromCartCallback,
  toggleWishListCallback,
  wishList,
  cart,
  closeButton,
}) {
  return (
    <>
      <h1>Cart</h1>
      {products.length === 0 && productInfoLoaded && <h2>No Items in Cart</h2>}
      <ProductList
        productInfoLoaded={productInfoLoaded}
        products={products}
        removeFromCartCallback={removeFromCartCallback}
        toggleWishListCallback={toggleWishListCallback}
        wishList={wishList}
        cart={cart}
        closeButton={closeButton}
      />
    </>
  );
}
