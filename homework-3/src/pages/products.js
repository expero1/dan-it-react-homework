import { ProductList } from "../components/product-list/product-list.jsx";
export default function Products({
  products,
  productInfoLoaded,
  addToCartCallback,
  removeFromCartCallback,
  toggleWishListCallback,
  wishList,
  cart,
}) {
  return (
    <>
      <ProductList
        products={products}
        productInfoLoaded={productInfoLoaded}
        addToCartCallback={addToCartCallback}
        removeFromCartCallback={removeFromCartCallback}
        toggleWishListCallback={toggleWishListCallback}
        wishList={wishList}
        cart={cart}
      />
    </>
  );
}
