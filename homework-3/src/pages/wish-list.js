import { ProductCardWishList } from "../components/wish-list-button/wish-list-button";
import { ProductCard } from "../components/product-card-info/product-card-info";
import { ProductList } from "../components/product-list/product-list";
// import { ProductListWithCloseButton } from "../components/product-list-with-close-button/product-list-with-close-button";

export default function WishList({
  productInfoLoaded,
  products,
  cart,
  wishList,
  toggleWishListCallback,
  addToCartCallback,
}) {
  return (
    <>
      <h1>Wish List</h1>
      {products.length === 0 && productInfoLoaded && "No Items In Wish List"}
      <ProductList
        products={products}
        productInfoLoaded={productInfoLoaded}
        addToCartCallback={addToCartCallback}
        toggleWishListCallback={toggleWishListCallback}
        cart={cart}
        wishList={wishList}
      />
    </>
  );
}
