import React from "react";
import "./product-list.scss";
import { ProductCard } from "../product-card/product-card";
import Loader from "../loader/loader";
export function ProductList({
  productInfoLoaded,
  products,
  addToCartCallback,
  removeFromCartCallback,
  wishList,
  cart,
  toggleWishListCallback,
  closeButton = false,
}) {
  return (
    <section>
      {productInfoLoaded ? (
        <ul className="product-list">
          {products.map((product) => (
            <li className="product-card" key={product.productSKU}>
              <ProductCard
                product={product}
                addToCartCallback={addToCartCallback}
                closeButtonCallback={removeFromCartCallback}
                toggleWishListCallback={toggleWishListCallback}
                isProductInWishList={wishList.includes(product.id)}
                isProductInCart={cart.includes(product.id)}
                closeButton={closeButton}
              />
            </li>
          ))}
        </ul>
      ) : (
        <Loader />
      )}
    </section>
  );
}
