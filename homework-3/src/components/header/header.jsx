import React from "react";
import "./header.scss";
import shoppingCart from "../../image/cart-shopping-solid.svg";
import starSolid from "../../image/star-solid.svg";
import bookOpen from "../../image/book-open-solid.svg";
import logo from "../../image/font-awesome-regular.svg";
import { Link, Outlet } from "react-router-dom";

export function Header({ cartQuantity, wishListQuantity }) {
  return (
    <section className="header">
      <div className="logo">
        <Link to="/">
          <img src={logo} alt="site logo" />
        </Link>
      </div>
      <div className="products">
        <Link to="/">
          <img src={bookOpen} alt="Product list" />
          Product List
        </Link>
      </div>
      <div className="cart">
        <Link to="/cart">
          <img src={shoppingCart} alt="Shopping Cart" />
          Cart:({cartQuantity ?? 0})
        </Link>
      </div>

      <div className="wish-list">
        <Link to="/wish-list">
          <img src={starSolid} alt="Wish List" />
          Wish List: ({wishListQuantity ?? 0})
        </Link>
      </div>

      <Outlet />
    </section>
  );
}
