import "./close-button.scss";
export const CloseButton = ({ className = "", onClick = () => {} }) => {
  return (
    <button className={`close-btn ${className}`} onClick={onClick}></button>
  );
};
