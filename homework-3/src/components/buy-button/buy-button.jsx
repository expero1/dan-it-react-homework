import { Button } from "../button/button";
export const BuyButton = ({ isProductInCart, addToCartCallback }) => {
  return (
    <>
      {isProductInCart ? (
        <Button backgroundColor="grey" text="Added To Cart" disabled={true} />
      ) : (
        <Button backgroundColor="red" text="Buy" onClick={addToCartCallback} />
      )}
    </>
  );
};
