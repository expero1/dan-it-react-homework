import React from "react";
import PropTypes from "prop-types";
import "./product-card-info.scss";

function ProductCardInfo({
  productImage,
  productName,
  productPrice,
  productSKU,
  productColor = "",
  // addToCartCallback,
}) {
  return (
    <>
      <div className="product-card__image">
        <img src={productImage} alt={productName} />
      </div>
      <h2 className="product-card__name">{productName}</h2>
      <ul className="product-card__props">
        <li className="product-card__price">Price: {productPrice} UAH</li>

        <li className="product-card__sku">SKU: {productSKU}</li>
        {productColor && (
          <li className="product-card__color">Color: {productColor}</li>
        )}
      </ul>
    </>
  );
}

ProductCardInfo.propTypes = {
  productId: PropTypes.symbol,
  productImage: PropTypes.string,
  productPrice: PropTypes.number,
  productSKU: PropTypes.string,
  productColor: PropTypes.string,
};
ProductCardInfo.defaultProps = {
  productImage: "https://placehold.co/600x400?text=No+Image",
  productColor: "",
};
export { ProductCardInfo };
