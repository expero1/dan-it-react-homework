import React from "react";
import "./button.scss";
export function Button({ backgroundColor, text, onClick, dataSet }) {
  return (
    <button
      className="btn"
      style={{ backgroundColor }}
      onClick={onClick}
      {...renderDataAttributes(dataSet)}>
      {text}
    </button>
  );
}

function renderDataAttributes(dataSet) {
  const renderedAttributes = {};
  for (let attribute in dataSet) {
    renderedAttributes[`data-${attribute}`] = dataSet[attribute];
  }
  return renderedAttributes;
}
