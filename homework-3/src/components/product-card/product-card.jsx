// import { Button } from "../button/button";
import { BuyButton } from "../buy-button/buy-button";
import { CloseButton } from "../close-button/close-button";
import { WishListButton } from "../wish-list-button/wish-list-button";
import { ProductCardInfo } from "../product-card-info/product-card-info";
import "./product-card.scss";
export const ProductCard = ({
  product,
  isProductInWishList,
  isProductInCart,
  toggleWishListCallback,
  addToCartCallback,
  closeButtonCallback,
  closeButton = false,
}) => {
  return (
    <>
      {closeButton && (
        <CloseButton
          className="product-card-close-btn close-btn-dark"
          onClick={() => {
            closeButtonCallback(product.id);
          }}
        />
      )}

      <ProductCardInfo {...product} />
      <WishListButton
        isProductInWishList={isProductInWishList}
        toggleWishListCallback={() => toggleWishListCallback(product.id)}
      />
      <BuyButton
        isProductInCart={isProductInCart}
        addToCartCallback={() => addToCartCallback(product.id)}
      />
    </>
  );
};
