import React from "react";
import starSolid from "../../image/star-solid.svg";
import starRegular from "../../image/star-regular.svg";
import "./wish-list-button.scss";
export function WishListButton({
  isProductInWishList,
  toggleWishListCallback,
}) {
  return (
    <span className="wishlist-btn" onClick={toggleWishListCallback}>
      {isProductInWishList ? (
        <span>
          <img src={starSolid} alt="product in wish list" />
          Added to wish list
        </span>
      ) : (
        <span>
          <img src={starRegular} alt="add to wish list" />
          Add to wish list
        </span>
      )}
    </span>
  );
}
