import React from "react";
import "./modal.scss";
import { CloseButton } from "../close-button/close-button";
function clickHandler(e, closeCallback) {
  if (
    e.target.classList.contains("modal") ||
    e.target.classList.contains("close-btn")
  ) {
    closeCallback();
  }
}

function renderActionButtonsSection(actionButtons) {
  return !actionButtons
    ? []
    : actionButtons.map((actionButton, index) => (
        <li key={index}>{actionButton}</li>
      ));
}
export function Modal({ header, text, closeCallback, closeButton, action }) {
  return (
    <div
      className="modal"
      onClick={(e) => {
        clickHandler(e, closeCallback);
      }}>
      <div className="modal__content">
        <div className="modal__header">
          {header}
          {closeButton && <CloseButton />}
        </div>
        <div className="modal__body">{text}</div>
        <ul className="modal-button__block">
          {[...renderActionButtonsSection(action)]}
        </ul>
      </div>
    </div>
  );
}
